<%@page contentType="text/html" pageEncoding="UTF-8"
    import ="java.sql.Connection"        
    import ="java.sql.DriverManager"        
    import ="java.sql.ResultSet"        
    import ="java.sql.Statement"        
    import ="java.sql.SQLException"
%>

<%
    Class.forName("com.mysql.cj.jdbc.Driver");
    Connection conex = (Connection)DriverManager.getConnection("jdbc:mysql://127.0.0.1/refacciones?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true&useSSL=false","root","Omar11Alex13");
    Statement sql = conex.createStatement();
    ResultSet data = sql.executeQuery("select * from refacciones where visibilidad=1");
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registros</title>
    </head>
    
    <body style="background-color:#000000">
        
        <h1 style="color:#ffffff">Registros</h1>
        
        <table style="width:100%; border:1px solid white; border-collapse:collapse ; align-content:flex-start">
            <tr>
                <th style="color:#ffffff; border:1px solid white">ID</th>
                <th style="color:#ffffff; border:1px solid white">Tipo</th>
                <th style="color:#ffffff; border:1px solid white">Nombre</th>
                <th style="color:#ffffff; border:1px solid white">Marca</th>
                <th style="color:#ffffff; border:1px solid white">Costo ($)</th>
                <th style="color:#ffffff; border:1px solid white">Armadora</th>
                <th style="color:#ffffff; border:1px solid white">Modelo</th>
                <th style="color:#ffffff; border:1px solid white">Año</th>
                <th style="color:#ffffff; border:1px solid white;">Fecha</th>
            </tr>
            
            <%
                while (data.next()){
            %>
            
            <tr>
                <td style="color:#ffffff; border:1px solid white;"><%= data.getInt("id")%></td>
                <td style="color:#ffffff; border:1px solid white;"><%= data.getString("tipo")%></td>
                <td style="color:#ffffff; border:1px solid white;"><%= data.getString("nombre")%></td>
                <td style="color:#ffffff; border:1px solid white;"><%= data.getString("marca")%></td>
                <td style="color:#ffffff; border:1px solid white;"><%= data.getFloat("costo")%></td>
                <td style="color:#ffffff; border:1px solid white;"><%= data.getString("armadora")%></td>
                <td style="color:#ffffff; border:1px solid white;"><%= data.getString("modelo")%></td>
                <td style="color:#ffffff; border:1px solid white;"><%= data.getString("año")%></td>
                <td style="color:#ffffff; border:1px solid white;"><%= data.getString("fecha")%></td>
            </tr>
            
            <%
                } data.close();
            %>
            
        </table><br>
        
        <a style="font-size:20px; color:#ffffff" href="editar.jsp">Editar</a><br><br>
        <a style="font-size:20px; color:#ffffff" href="eliminar.jsp">Eliminar</a><br><br>
        <a style="font-size:20px; color:#ffffff" href="index.html">Regresar</a>
        
    </body>
    
</html>

